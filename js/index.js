
    	$(function (){
    		$('[data-toggle="tooltip"]').tooltip();
    		$('[data-toggle="popover"]').popover();
    		$('.carousel').carousel({
    			interval: 4000
    		});

    		$('#registro').on('show.bs.modal' ,function(e){
    			console.log ('el modal se está mostrando');

    			$('#registrobtn').removeClass('btn-outline-success');
    			$('#registrobtn').addClass('btn-primary');
    			$('#registrobtn').prop('disabled', true);

    		});

    		$('#registro').on('shown.bs.modal' ,function(e){
    			console.log ('el modal se está mostró');

    		});
    		$('#registro').on('hide.bs.modal' ,function(e){
    			console.log ('el modal se oculta');

    		});
    		$('#registro').on('hidden.bs.modal' ,function(e){
    			console.log ('el modal se ocultó');
    			$('#registrobtn').prop('disabled', false);

    		});


    	});
